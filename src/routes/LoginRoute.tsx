import React, {Component, useState} from "react";
import {Redirect, Route, RouteProps, useHistory} from "react-router-dom";
import {useSelector} from "react-redux";
import {AppState} from "store/types";
import {userIsAuth, userSelector} from "store/selectors/auth";
const Login = React.lazy(() => import('pages/Login'))

const LoginRoute = <T extends RouteProps>(props: T) => {
    console.log("LOGIN ROUTE INVOKED")

    const isAuth = useSelector<AppState>(state => userIsAuth(state))
    const user = useSelector<AppState>(state => userSelector(state))

    console.log(user)
    return (
        <Route
            {...props}
            render={({location}: any) => {
                const pathname: string = location?.state?.from?.pathname === '/login'
                    ? '/' : location?.state?.from?.pathname

                console.log("COMING FROM PATH = ", pathname)

                if (isAuth) {
                    return (
                        <Redirect to={{
                            pathname,
                            state: {from: location}
                        }}/>
                    )
                }

                return <Login/>
            }}
        />
    )
}

export default LoginRoute;
