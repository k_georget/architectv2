import React from 'react'
import { Link, NavLink, Route, Switch, Redirect } from 'react-router-dom'
import Home from '../pages/home'
import LoginRoute from './LoginRoute'
import PrivateRoute from 'routes/PrivateRoute'

type PrivatePath =
    | '/'
    | '/estimates'
    | '/estimates/new'
    | '/search'
    | '/catalog'
    | '/profile'
type PublicPath = '/login' | '/about' | '/public' | '*'

export interface AppRoute {
    label: string
    path: PublicPath | PrivatePath
    page: React.FC | React.ReactNode
    isPrivate?: boolean
    exact?: boolean
}

export function isValidAppRoute(route: any): route is AppRoute {
    return (
        (route as AppRoute).label !== undefined &&
        (route as AppRoute).path !== undefined &&
        (route as AppRoute).page !== undefined
    )
}

export const Routes = () => {
    return (
        <Switch>
            <LoginRoute path="/login" />
            <Route path="/about">
                <div>
                    <div>ABOUT ///</div>
                    <Link to="/">-> HOME</Link>
                    <div>CARAAAY</div>
                    <span> SPANU </span>
                    <span> SPANO </span>
                    <div>CARRRRRRAY</div>
                    <span>SPANI</span>
                </div>
            </Route>
            <Route path="/public">
                <div className="flex flex-col">
                    <div>PUBLIC</div>
                    <Link to="/about">-> ABOUT</Link>
                    <NavLink
                        activeClassName="active"
                        isActive={(location) => true}
                        to="/">
                        NAV LINK TO HOME :)
                    </NavLink>
                </div>
            </Route>
            <PrivateRoute path="/">
                <Home />
            </PrivateRoute>
            <Route path="*">404 NOT FOUND</Route>
        </Switch>
    )
}
