import React from "react";
import {Redirect, Route, RouteProps} from "react-router-dom";
import {useSelector} from "react-redux";
import {userIsAuth} from "store/selectors/auth";
import {AppState} from "store/types";

const PrivateRoute: React.FC<RouteProps> = (props) => {
    const {children, ...otherRouteProps} = props

    const isAuth = useSelector<AppState>(state => userIsAuth(state))

    return (
        <Route
            {...otherRouteProps}
            render={({location}) => {
                const notAuthenticatedLocation = {
                    pathname: '/login',
                    state: {
                        from: location
                    }
                }
                console.log("iS AUTH ??: ", isAuth)
                return isAuth ? (
                    children
                ) : (
                    <Redirect to={notAuthenticatedLocation}/>
                )
            }}
        />
    )
}

export default PrivateRoute;
