import React from 'react';
import {persistor, store} from "store";
import {PersistGate} from "redux-persist/integration/react";
import {Provider} from "react-redux";
import App from "App";

const AppContainer = () => {
    return (
        <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
                <App/>
            </PersistGate>
        </Provider>
    )
}

export default AppContainer;
