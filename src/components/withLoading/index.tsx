import React from 'react'
import { useSelector } from 'react-redux'
import { AppState } from 'store/types'
import LoadingIndicator, {LoadingInsetY, LoadingPosition} from 'components/loadingIndicators/LoadingIndicator'
import { PersistPartial } from 'redux-persist/lib/persistReducer'

interface Loadable {}

export type WithLoading = Loadable

interface WithLoadingParams {
    loadingWhat?: keyof AppState,
    position?: LoadingPosition,
    color?: string,
    insetY?: LoadingInsetY
    size?: string
}

export const withLoading = (params: WithLoadingParams) => <
    OwnProps extends Loadable = Loadable
>(
    BaseComponent: React.ComponentType<OwnProps>
) => {
    let { loadingWhat, color, position, insetY, size } = params

    return (props: OwnProps) => {
        if (typeof loadingWhat === 'undefined') {
            loadingWhat = 'ui'
        }

        const loading = useSelector(
            (state: AppState) => state[loadingWhat as keyof AppState]?.loading
        )

        return (
            <>
                <BaseComponent {...(props as OwnProps)} />
                {loading && (
                    <LoadingIndicator
                        position={position}
                        color={color}
                        insetY={insetY}
                        size={size}
                    />
                )}
            </>
        )
    }
}
