import React, { ChangeEvent } from 'react'

type BaseSelectProps = {
    children: JSX.Element[] | JSX.Element | Element[] | Element
    onChange: (e: ChangeEvent<HTMLSelectElement>) => void
    disabled?: boolean
    value?: string | number
}

const BaseSelect = ({ children, ...rest }: BaseSelectProps) => {
    return (
        <div className="relative">
            <select
                {...rest}
                className="w-full bg-white border-2 border-gray-500 text-gray-darkest font-main py-3 px-4 pr-8 rounded leading-tight
                focus:outline-none">
                {children}
            </select>
            <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                <svg
                    className="fill-current h-4 w-4"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 20 20">
                    <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                </svg>
            </div>
        </div>
    )
}

export default BaseSelect
