import React, {useState} from 'react';
import {Card, CardContent, CardHeader, Collapse} from "@material-ui/core";
import {Status} from "services/types";

interface EstimateItemProps {
    className?: string
    customerFullName: string
    projectName: string
    line: string
    totalPriceWithVAT: number
    statusSummary: Status,
    customerValidationStatus: Status
    marketingValidationStatus: Status
    designOfficeValidationStatus: Status
}

const EstimateItem: React.FC<EstimateItemProps> = (props: EstimateItemProps) => {
    const {
        className,
        customerFullName,
        projectName,
        line,
        totalPriceWithVAT,
        statusSummary,
        customerValidationStatus,
        marketingValidationStatus,
        designOfficeValidationStatus
    } = props;

    const [expanded, setExpanded] = useState<boolean>(false);

    interface EstimateStatusProps {
        status: Status
    }

    const EstimateStatus: React.FC<EstimateStatusProps> = ({status}) => {
        console.log(status)
       return <div className={"rounded-full h-6 w-6 bg-" + status}/>
    }


    return <>
        <Card className={className}>
            <CardHeader
                className="flex-row-reverse"
                classes={{
                    title: "font-title text-xl",
                    subheader: "font-main text-md",
                }}
                avatar={
                    <EstimateStatus status={statusSummary}/>
                }
                title={projectName}
                subheader={customerFullName}
            />
            <CardContent>
                <div className="flex justify-between">
                    <div className="font-title">Gamme</div>
                    <div>{line}</div>
                </div>
                <div className="flex justify-between">
                    <div className="font-title">Prix TTC</div>
                    <div>{totalPriceWithVAT} € TTC</div>
                </div>
                <div className="flex justify-between">
                    <div className="font-title">Status</div>
                    <div className="flex justify-between">
                        <div>{"en cours"}</div>
                        <button
                            onClick={() => setExpanded(!expanded)}
                            aria-expanded={expanded}
                            aria-label="show more"
                        >
                            Détails
                        </button>
                    </div>
                </div>
                <Collapse in={expanded} timeout="auto" unmountOnExit>
                    <div>{customerValidationStatus}</div>
                    <div>{marketingValidationStatus}</div>
                    <div>{designOfficeValidationStatus}</div>
                </Collapse>
            </CardContent>
        </Card>
    </>
}

export default EstimateItem;

