import React from 'react'
import { PacmanLoader } from 'react-spinners'
import {themeColorToHexColor} from "helpers/resolveTailwindConfig";;

export type LoadingPosition = 'absolute' | 'fixed' | 'relative'
export type LoadingInsetY =
    | '0'
    | 'auto'
    | '1/2'
    | '1/12'
    | '2/12'
    | '3/12'
    | '4/12'
    | '5/12'
    | '6/12'
    | '7/12'
    | '8/12'
    | '9/12'
    | '10/12'
    | '11/12'

type LoadingIndicatorProps = {
    position?: LoadingPosition
    insetY?: LoadingInsetY
    color?: string
    size?: string
}

const LoadingIndicator = (props: LoadingIndicatorProps) => {
    const { position, insetY, color, size } = props

    const hexColor = themeColorToHexColor(color)

    return (
        <div className={`${position} inset-y-${insetY} z-20 inset-x-1/2`}>
            <PacmanLoader size={size} color={hexColor} />
        </div>
    )
}

LoadingIndicator.defaultProps = {
    position: 'fixed',
    insetY: '1/2',
    size: '2vh',
    color: 'primary'

}

export default LoadingIndicator


