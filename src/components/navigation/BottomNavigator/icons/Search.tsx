import * as React from "react"
import {ICON_SIZE, IconProps} from "./constants";

export default ({active}: IconProps) => {
    return (
            <svg
                className={active ? "stroke-primary fill-none transition-stroke duration-1000" : "stroke-current fill-none"}
                width={ICON_SIZE}
                height={ICON_SIZE}
                viewBox="0 0 24 24"
                strokeWidth={2}
                strokeLinecap="round"
                strokeLinejoin="round"
            >
                <circle cx={11} cy={11} r={8} />
                <path
                    d="M21 21l-4.35-4.35"
                />
            </svg>
        )
}

