import * as React from "react"
import {ICON_SIZE, IconProps} from "./constants";

export default ({active}: IconProps) => {
    return (
        <svg
            className={active ? "stroke-primary fill-primary transition-fill duration-700" : "stroke-current fill-none"}
            width={ICON_SIZE}
            height={ICON_SIZE}
            viewBox="0 0 24 24"
            strokeWidth={2}
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <path d="M20 21v-2a4 4 0 00-4-4H8a4 4 0 00-4 4v2" />
            <circle cx={12} cy={7} r={4} />
        </svg>
    )
}

