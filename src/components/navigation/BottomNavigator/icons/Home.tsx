import * as React from "react"
import {ICON_SIZE, IconProps} from "./constants";

export default ({active}: IconProps) => {
    return (
        <svg
            className={active ? "stroke-primary fill-primary transition-fill duration-700" : "stroke-current fill-none"}
            width={ICON_SIZE}
            height={ICON_SIZE}
            viewBox="0 0 24 24"
            strokeWidth={2}
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <path d="M3 9l9-7 9 7v11a2 2 0 01-2 2H5a2 2 0 01-2-2z"/>
            <path
                className={active ? "stroke-white fill-primary transition-stroke duration-1000" : "stroke-current"}
                fill="none"
                strokeWidth={2}
                d="M9 22V12h6v10"
            />
        </svg>
    )
}

