export const ICON_SIZE = 24;

export interface IconProps {
    active?: boolean
}
