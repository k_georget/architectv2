import * as React from "react"
import {ICON_SIZE, IconProps} from "./constants";

export default ({active}: IconProps) => {
    return (
        <svg
            className={active ? "stroke-white fill-primary transition-fill duration-700" : "stroke-current fill-none"}
            width={ICON_SIZE}
            height={ICON_SIZE}
            viewBox="0 0 24 24"
            strokeWidth={2}
            strokeLinecap="round"
            strokeLinejoin="round"
        >
            <path d="M2 3h6a4 4 0 014 4v14a3 3 0 00-3-3H2zM22 3h-6a4 4 0 00-4 4v14a3 3 0 013-3h7z" />
        </svg>
    )
}

