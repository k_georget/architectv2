import React, {useState} from 'react';
import {Link, useRouteMatch} from "react-router-dom";
import {BottomNavigation, BottomNavigationAction} from "@material-ui/core";
import {AppRoute} from "routes/Routes";
import Home from "./icons/Home";
import Search from "./icons/Search";
import Profile from "./icons/Profile";
import Catalog from "./icons/Catalog";

interface BottomNavigatorProps {
    routes: Array<AppRoute>,
    className: string,
    tabClassName?: string
    tabLabelClassName?: string
}

const BottomNavigator: React.FC<BottomNavigatorProps> = (props) => {
    const {routes, className, tabLabelClassName} = props;

    let {path, url} = useRouteMatch();
    const [activeTab, setActiveTab] = useState<number>(0)

    console.log('USE ROUTE MATCH IN BOTTOM NAV', path, url);

    const tabIcons = [
        Home,
        Search,
        Catalog,
        Profile
    ]

    const tabs = routes.map((mainNavRoute, index) => {

        const getIcon = () => {
            return (
                <div className="mb-1">
                    {React.createElement(tabIcons[index], {active: index === activeTab})}
                </div>
            )
        }
            return (
                <BottomNavigationAction
                    classes={{
                        label: tabLabelClassName || ""
                    }}
                    label={mainNavRoute.label}
                    icon={getIcon()}
                    key={mainNavRoute.path}
                    component={Link}
                    to={mainNavRoute.path}
                    href={mainNavRoute.path}
                />
            )
        }
    )

    const getTranslateValue = () => {
        if (activeTab === 0) {
            return activeTab.toString()
        }

        return activeTab === 1 ? 'full' : (activeTab.toString() + 'full');
    }

    return <>
        <div className={className}>
            <BottomNavigation
                className="flex justify-around pt-4"
                value={activeTab}
                onChange={(event, newActiveTab) => {
                    setActiveTab(newActiveTab);
                }}
                showLabels={false}
            >
                {tabs}
            </BottomNavigation>
            <div
                className={"absolute bottom-0 bg-primary h-1 w-1/" + (tabs.length) +
                " transition duration-300 transform translate-x-" + getTranslateValue()}
            />
        </div>
    </>
}


export default BottomNavigator;

