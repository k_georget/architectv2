// import useState next to FunctionComponent
import React, {ReactElement} from 'react';

type TwHeight = '10' | '12' | '16' | '20' | '24' | '32' | '40' | '48' | '56' | '64' | 'full' | 'screen';

interface HeaderProps {
    height?: TwHeight,
    smHeight?: TwHeight
    mdHeight?: TwHeight
    lgHeight?: TwHeight
    xlHeight?: TwHeight
    leftClassName?: string,
    centerClassName?: string,
    rightClassName?: string,
    left?: ReactElement
    center: ReactElement | string
    right?: ReactElement
}

const Header: React.FC<HeaderProps> = (props) => {
    const {
        height,
        smHeight,
        mdHeight,
        lgHeight,
        xlHeight,
        leftClassName,
        centerClassName,
        rightClassName,
        left,
        center,
        right
    } = props;

    const DEFAULT_HEIGHT = '24';
    const heights = 'h-' + (height || DEFAULT_HEIGHT)
        + ' sm:h-' + (smHeight || DEFAULT_HEIGHT)
        + ' md:h-' + (mdHeight || DEFAULT_HEIGHT)
        + ' lg:h-' + (lgHeight || DEFAULT_HEIGHT)
        + ' xl:h-' + (xlHeight || DEFAULT_HEIGHT)

    return <>
        <header className={`flex bg-gray-100 w-full shadow-lg ${heights}`}>
            <div className={(leftClassName || "") + " flex w-3/12 h-full items-center"}>{left}</div>
            <div className={(centerClassName || "") + " flex w-6/12 h-full items-center"}>{center}</div>
            <div className={"flex h-full items-center " + (rightClassName || "")}>{right}</div>
        </header>
    </>
}

export default Header;
