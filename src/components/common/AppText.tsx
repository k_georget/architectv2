import React from "react";

type TextSize = 'xs' | 'sm' | 'md' | 'lg' | 'xl';

type AppTextProps = {
    size?: TextSize,
    color?: string,
    className?: string
}

const defaultProps: Partial<AppTextProps> = {
    size: 'sm',
};

const AppText: React.FC<AppTextProps> = (props) => {
    const {
        children,
        size,
        color,
        className
    } = props;

    const sizes = {
        xs: 'text-xs lg:text-sm xl:text-xl 2xl:text-2xl 3xl:text-3xl',
        sm: 'text-sm lg:text-md xl:text-xl 2xl:text-2xl 3xl:text-3xl',
        md: 'text-md lg:text-lg xl:text-xl 2xl:text-2xl 3xl:text-3xl',
        lg: 'text-lg lg:text-xl xl:text-2xl 2xl:text-3xl 3xl:text-4xl',
        xl: 'text-xl lg:text-2xl xl:text-3xl 2xl:text-4xl 3xl:text-5xl'
    }

    const composedClassName = `${sizes[size || 'sm']} text-${color} ${className || ''}`


    const testclass = <div className=''></div>

    return (
        <div className={composedClassName}>{children}</div>
    );
};

AppText.defaultProps = defaultProps;

export default AppText;
