import React from 'react'
import AppText from "components/common/AppText";

type BaseButtonProps = {
    type?: 'submit' | 'button' | 'reset'
    disabled?: boolean
    label: string
    onClick?: (param?: any) => any
    actionType?: 'primary' | 'secondary'
    width?: string
    height?: string
}

const BaseButton = (props: BaseButtonProps) => {
    const { label, actionType, width, height, ...rest } = props

    let className
    if (actionType === 'primary') {
        className = `bg-primary text-white focus:bg-white focus:text-primary disabled:opacity-50`
    }

    if (actionType === 'secondary') {
        className = `bg-transparent text-primary border-2 border-primary focus:bg-primary focus:text-white`
    }

    return (
        <button
            {...rest}
            className={`${className} h-${height} w-${width}
            font-button py-2 px-1 mx-2 rounded md:h-12 lg-h-12 xl:h-12`}>
            <AppText size="sm" className="mx-4">{label}</AppText>
        </button>
    )
}

BaseButton.defaultProps = {
    type: 'button',
    disabled: false,
    actionType: 'primary',
    //width: '2/12',
    //height: '8',
}

export default BaseButton
