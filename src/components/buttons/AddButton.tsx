import React from 'react';

type AddButtonProps = {
    onClick(): void
}

const AddButton = (props: AddButtonProps) => {

    const {onClick} = props

    return (
        <button
            className={`h-12 w-12 flex justify-center items-center bg-primary
                active:bg-yellow-500 rounded-full shadow-md`}
            onClick={onClick}>
            <svg
                width={25}
                height={25}
                viewBox="0 0 25 25"
                strokeWidth={2}
                strokeLinecap="round"
                strokeLinejoin="round"
                className="stroke-white fill-none"
            >
                <path d="M12 5v14M5 12h14" />
            </svg>
        </button>
    )

}

export default AddButton
