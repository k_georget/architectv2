import React from 'react'

type NextButtonProps = {
    //onClick?: () => void
    type?: 'submit' | 'button' | 'reset'
    disabled?: boolean
}

const NextButton = (props: NextButtonProps) => {
    return (
        <button
            type="submit"
            {...props}
            className={`h-12 w-16 flex justify-center items-center bg-primary
                active:bg-yellow-500 hover:bg-secondary rounded-full shadow-md`}>
            <svg
                width={25}
                height={25}
                viewBox="0 0 640 640"
                shapeRendering="geometricPrecision"
                textRendering="geometricPrecision"
                imageRendering="optimizeQuality"
                fill="white"
                fillRule="evenodd"
                clipRule="evenodd"
                {...props}>
                <path d="M640.012 319.953L394.729 74.788H163.266l163.36 163.361H-.012v163.702h326.638l-163.36 163.361h231.463z" />
            </svg>
        </button>
    )
}

NextButton.defauktProps = {
    //onClick: () => {},
    type: 'submit',
    disabled: false,
}

export default NextButton
