import React from "react";

export class MouseTracker extends React.Component {
    constructor(props) {
        super(props);
        this.handleMouseMove = this.handleMouseMove.bind(this);
        this.state = { x: 0, y: 0 };
    }

    handleMouseMove(event) {
        this.setState({
            x: event.clientX,
            y: event.clientY
        });
    }

    render() {
        return (
            <div onMouseMove={this.handleMouseMove}>
                <h1>Déplacez votre souris sur l’écran !</h1>
                <p>La position actuelle de la souris est ({this.state.x}, {this.state.y})</p>
            </div>
        );
    }
}
