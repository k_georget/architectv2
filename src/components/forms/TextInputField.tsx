import React from 'react'
import { useField } from 'formik'
import AppText from "components/common/AppText";

export type InputProps = {
    //id: string,
    name: string
    //value: string,
    type: string
    label: string
    //placeholder: string,
    //onChange: (e: React.ChangeEvent<HTMLInputElement>) => void,
    //onBlur: (e: React.FocusEvent<HTMLInputElement>) => void
}


const FieldErrorMsg = ({children}: any) => {
    return (
        <AppText
            size="sm"
            color="refused"
        >{children}
        </AppText>
    )
}

export const TextInputField: React.FC<InputProps> = (props: InputProps) => {
    const { name, label, type } = props

    const [field, meta, helpers] = useField(props)
   // console.log(props, field, meta, helpers)

    const hasError = () => {
        return meta.touched && meta.error
    }

    let inputClassName = `h-12 pl-2 text-sm focus:bg-yellow-100 bg-gray-100 border-b-2 ${hasError() ? ' border-refused' : 'border-primary'}`

    return (
        <div className="h-24">
            <label className="flex flex-col">
                <AppText className="font-bold p-2">{label}</AppText>
                <input className={inputClassName} {...field} {...props} />
            </label>
            {meta.touched && meta.error && <FieldErrorMsg>{meta.error}</FieldErrorMsg>}
        </div>
    )
}
