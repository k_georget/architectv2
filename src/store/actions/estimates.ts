import {FETCH_ALL_ESTIMATES, FETCH_ALL_ESTIMATES_ERROR, FETCH_ALL_ESTIMATES_SUCCESS} from "store/types";
import {IEstimate} from "services/types";
import {IException} from "exceptions/Exception";

export const fetchAllEstimates = () => {
    return {
        type: FETCH_ALL_ESTIMATES
    }
}

export const fetchEstimatesSuccess = (estimates: IEstimate[]) => {
    return {
        type: FETCH_ALL_ESTIMATES_SUCCESS,
        estimates
    }
}

export const fetchEstimatesError = (error: IException) => {
    return {
        type: FETCH_ALL_ESTIMATES_ERROR,
        error
    }
}
