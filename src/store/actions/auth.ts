import {
    AuthActionTypes,
    REQUEST_SIGN_IN,
    SIGN_IN_SUCCESS,
    SIGN_IN_ERROR,
    RequestSignIn, REQUEST_SIGN_OUT, RequestSignOut,
} from 'store/types'
import { IException } from 'exceptions/Exception'
import {ITokenBag, IUser, UserCredentials} from 'services/types'

export const requestSignIn = (
    credentials: UserCredentials
): RequestSignIn => {
    return {
        type: REQUEST_SIGN_IN,
        credentials,
    }
}

export const signInSuccess = (
    user: IUser,
    tokenBag: ITokenBag
): AuthActionTypes => {
    return {
        type: SIGN_IN_SUCCESS,
        user,
        tokenBag,
    }
}

export const requestSignOut = (): AuthActionTypes => {
    return {
        type: REQUEST_SIGN_OUT
    }
}

export const signInError = (error: IException): AuthActionTypes => {
    return {
        type: SIGN_IN_ERROR,
        error,
    }
}
