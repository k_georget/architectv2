import { IException } from 'exceptions/Exception'
import reducers from "store/reducers";
import {IEstimate, ITokenBag, IUser, UserCredentials} from "services/types";

export type AppState = ReturnType<typeof reducers>

export interface UIState {
    showMainAlertNotification: boolean
    loading: boolean
}

////// REDUCERS ///////
export interface AuthState {
    loading: boolean
    user: IUser | null
    tokenBag: ITokenBag | null
    error: IException | null
}

export interface EstimatesState {
    loading: boolean
    all: IEstimate[]
    details: IEstimate | null
    error: IException | null
}

////// ACTIONS ///////

/// APP INIT ///
export const REQUEST_INIT_APPLICATION = 'REQUEST_INIT_APPLICATION'
export const INIT_APPLICATION_SUCCESS = 'INIT_APPLICATION_SUCCESS'
export const INIT_APPLICATION_ERROR = 'INIT_APPLICATION_ERROR'

export interface RequestInitApplication {
    type: typeof REQUEST_INIT_APPLICATION
}

export interface InitApplicationSuccess {
    type: typeof INIT_APPLICATION_SUCCESS
}

export interface InitApplicationError {
    type: typeof INIT_APPLICATION_ERROR
}

export type InitApplicationActionType = RequestInitApplication | InitApplicationSuccess | InitApplicationError

/// AUTH ///
export const REQUEST_SIGN_IN = 'REQUEST_SIGN_IN'
export const SIGN_IN_SUCCESS = 'SIGN_IN_SUCCESS'
export const SIGN_IN_ERROR = 'SIGN_IN_ERROR'

export const REQUEST_SIGN_OUT = 'REQUEST_SIGN_OUT'

export interface RequestSignIn {
    type: typeof REQUEST_SIGN_IN
    credentials: UserCredentials
}

interface SignInSuccess {
    type: typeof SIGN_IN_SUCCESS
    user: IUser,
    tokenBag: ITokenBag
}

interface SignInError {
    type: typeof SIGN_IN_ERROR
    error: IException
}

export interface RequestSignOut {
    type: typeof REQUEST_SIGN_OUT
}

export type AuthActionTypes = RequestSignIn | SignInSuccess | SignInError | RequestSignOut

/// ESTIMATES ///
export const FETCH_ALL_ESTIMATES = 'FETCH_ALL_ESTIMATES'
export const FETCH_ALL_ESTIMATES_SUCCESS = 'FETCH_ALL_ESTIMATES_SUCCESS'
export const FETCH_ALL_ESTIMATES_ERROR = 'FETCH_ALL_ESTIMATES_ERROR'

export interface FetchAllEstimates {
    type: typeof FETCH_ALL_ESTIMATES
}

export interface FetchAllEstimatesSuccess {
    type: typeof FETCH_ALL_ESTIMATES_SUCCESS
    estimates: IEstimate[]
}

export interface FetchAllEstimatesError {
    type: typeof FETCH_ALL_ESTIMATES_ERROR
    error: IException
}

export type EstimatesActionTypes = FetchAllEstimates | FetchAllEstimatesSuccess | FetchAllEstimatesError
