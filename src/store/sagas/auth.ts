import { call, apply, put, takeLatest, select } from 'redux-saga/effects'
import {requestSignIn, signInError, signInSuccess} from 'store/actions/auth'
import {AuthApi} from "services/auth/AuthApi";
import {UserApi} from "services/user/UserApi";
import {REQUEST_SIGN_IN} from "store/types";

export function* requestSignInWatcher() {
    yield takeLatest(REQUEST_SIGN_IN, signIn)
}

function* signIn({ credentials }: ReturnType<typeof requestSignIn>) {
    try {
        const tokenBag = yield call([new AuthApi(), 'signIn'], credentials)
        console.log('Retrieved token bag = ', tokenBag)
        const user = yield apply(new UserApi(tokenBag), 'me', [])
        yield put(signInSuccess(user, tokenBag))
    } catch (e) {
        console.error('Error in requestSignIn saga: ', e)
        yield put(signInError(e))
        // TODO ERROR HANDLER

    }
}
