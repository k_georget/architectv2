import { FETCH_ALL_ESTIMATES } from 'store/types'
import { apply, put, takeLatest, select } from 'redux-saga/effects'
import { tokenBagSelector } from 'store/selectors/auth'
import { EstimatesApi } from 'services/estimates/EstimatesApi'
import {
    fetchEstimatesError,
    fetchEstimatesSuccess,
} from 'store/actions/estimates'

export function* fetchEstimatesWatcher() {
    yield takeLatest(FETCH_ALL_ESTIMATES, fetchEstimates)
}

function* fetchEstimates() {
    try {
        console.log('FETCHING ESTIMOUSSE')
        const tokenBag = yield select(state => tokenBagSelector(state))
        console.log('Retrieved token bag = ', tokenBag)

        const estimates = yield apply(new EstimatesApi(tokenBag), 'getAll', [])

        console.log('ESTIMATES MAMEN !', estimates)
        yield put(fetchEstimatesSuccess(estimates))
    } catch (e) {
        console.error('Error in fetchEstimates saga: ', e)
        yield put(fetchEstimatesError(e))
        // TODO ERROR HANDLER
    }
}
