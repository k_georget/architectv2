import { all } from 'redux-saga/effects'
import { requestSignInWatcher } from 'store/sagas/auth'
import {fetchEstimatesWatcher} from "store/sagas/estimates";

export default function* rootSaga() {
    yield all([
        requestSignInWatcher(),
        fetchEstimatesWatcher(),
    ])
}
