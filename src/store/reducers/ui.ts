import { Reducer } from 'redux'
import { UIState } from 'store/types'

let initialState: UIState = {
    showMainAlertNotification: false,
    loading: false,
}

export const ui: Reducer<UIState, any> = (
    state = initialState,
    action: any
): UIState => {
    switch (action.type) {
        default:
            return state
    }
}
