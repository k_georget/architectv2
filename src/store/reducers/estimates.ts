import {Reducer} from 'redux'
import {
    EstimatesActionTypes,
    EstimatesState,
    FETCH_ALL_ESTIMATES,
    FETCH_ALL_ESTIMATES_ERROR,
    FETCH_ALL_ESTIMATES_SUCCESS,
} from 'store/types'

export const initialState: EstimatesState = {
    loading: false,
    all: [],
    details: null,
    error: null
}

export const estimates: Reducer<EstimatesState, EstimatesActionTypes> = (
    state= initialState,
    action
): EstimatesState => {
    switch (action.type) {
        case FETCH_ALL_ESTIMATES:
            return {
                ...state,
                loading: true,
            }
        case FETCH_ALL_ESTIMATES_SUCCESS:
            return {
                ...state,
                loading: false,
                all: action.estimates
            }
        case FETCH_ALL_ESTIMATES_ERROR:
            return {
                ...state,
                loading: false,
                error: action.error
            }
        default:
            return state
    }
}
