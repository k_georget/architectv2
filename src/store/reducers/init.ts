import { Reducer } from 'redux'
import {
    INIT_APPLICATION_ERROR,
    INIT_APPLICATION_SUCCESS,
    REQUEST_INIT_APPLICATION,
    REQUEST_SIGN_OUT,
} from 'store/types'

const initialState: any = {
    loading: false,
}

export const init: Reducer<any, any> = (
    state = initialState,
    action: any
): any => {
    switch (action.type) {
        case REQUEST_INIT_APPLICATION:
            return {
                ...state,
                loading: true,
            }
        case INIT_APPLICATION_SUCCESS:
            return {
                ...state,
                user: action.user,
                tokenBag: action.tokenBag,
                loading: false,
            }
        case INIT_APPLICATION_ERROR:
            return {
                ...state,
                error: action.error,
                loading: false,
            }
        case REQUEST_SIGN_OUT:
            return {
                ...state,
                user: null,
                tokenBag: null,
            }
        default:
            return state
    }
}
