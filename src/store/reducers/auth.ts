import {Reducer} from 'redux'
import {
    AuthActionTypes,
    AuthState,
    REQUEST_SIGN_IN,
    REQUEST_SIGN_OUT,
    SIGN_IN_ERROR,
    SIGN_IN_SUCCESS,
} from 'store/types'

const initialState: AuthState = {
    loading: false,
    error: null,
    tokenBag: null,
    user: null,
}

export const auth: Reducer<AuthState, AuthActionTypes> = (
    state = initialState,
    action
): AuthState => {
    switch (action.type) {
        case REQUEST_SIGN_IN:
            return {
                ...state,
                loading: true,
            }
        case SIGN_IN_SUCCESS:
            return {
                ...state,
                user: action.user,
                tokenBag: action.tokenBag,
                loading: false
            }
        case SIGN_IN_ERROR:
            return {
                ...state,
                error: action.error,
                loading: false
            }
        case REQUEST_SIGN_OUT:
            return {
                ...state,
                user: null,
                tokenBag: null
            }
        default:
            return state
    }
}
