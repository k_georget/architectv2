import { createTransform, persistReducer } from 'redux-persist'
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2'
import { combineReducers } from 'redux'
import { auth } from 'store/reducers/auth'
import storage from 'redux-persist/lib/storage'
import { ui } from 'store/reducers/ui'
import { AuthActionTypes, AuthState } from 'store/types'
import { User } from 'services/user/User'
import { estimates } from 'store/reducers/estimates'
import { TokenBag } from 'services/auth/TokenBag'
import { IUser } from 'services/types'

const UserTransform = createTransform(
    null,
    (user: any, key) => {
        if (user !== null) {
            return User.hydrate(user)
        }

        return user
    },
    { whitelist: ['user'] }
)

const TokenBagTransform = createTransform(
    null,
    (tokenBag: any, key) => {
        if (tokenBag !== null) {
            return TokenBag.hydrate(tokenBag)
        }

        return tokenBag
    },
    { whitelist: ['tokenBag'] }
)

const authPersistConfig = {
    key: 'auth',
    storage,
    stateReconciler: autoMergeLevel2,
    blacklist: ['loading', 'error'],
    transforms: [UserTransform, TokenBagTransform],
}

const reducers = combineReducers({
    ui: ui,
    auth: persistReducer<AuthState, AuthActionTypes>(authPersistConfig, auth),
    estimates: estimates,
    /*'network': network,
    'queue': persistReducer(queuePersistConfig, queue),
    'bookingCart': bookingCart,
    'bookings': bookings,
    'bookingDetails': bookingDetails,
    'connectedBooking': persistReducer(connectedBookingPersistConfig, connectedBooking),
    'vehicleInventory': persistReducer(vehicleInventoryPersistConfig, vehicleInventory),*/
})

export default reducers
