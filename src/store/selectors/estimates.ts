import { AppState } from 'store/types'
import {IEstimate} from "services/types";

export const allEstimatesSelector = (state: AppState) => {
    console.log("ALL ESTIM SELECTOR",state.estimates.all)
    return state.estimates.all
}

export const isFetchingEstimates = (state: AppState) => {
    return state.estimates.loading
}
