import { AppState } from 'store/types'
import { User } from 'services/user/User'
import { ITokenBag } from 'services/types'

export const userSelector = (state: AppState) => {
    return state.auth.user
}

export const userIsAuth = (state: AppState) => {
    return (
        state.auth.user !== null &&
        state.auth.tokenBag !== null &&
        !state.auth.tokenBag.hasAnExpiredAccessToken()
    )
}

export const tokenBagSelector = (state: AppState): ITokenBag | null => {
    return state.auth.tokenBag
}
