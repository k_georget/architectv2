import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { persistStore } from 'redux-persist'
import rootSaga from 'store/sagas'
import reducers from 'store/reducers'

//const loggerMiddleware = createLogger()
const sagaMiddleware = createSagaMiddleware()

export const store = createStore(
    reducers,
    applyMiddleware(
        sagaMiddleware
        //loggerMiddleware
    )
)

export const persistor = persistStore(store)

sagaMiddleware.run(rootSaga)
