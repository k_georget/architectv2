export interface IException {
    name: string,
    message: string
    getName(): string
}

export class Exception extends Error {
    constructor(name: string, message: string) {
        super(message)
        this.name = name
    }

    getName() {
        return this.name
    }
}
