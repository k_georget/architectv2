import React, { Suspense, useEffect, useState } from 'react'
import { BrowserRouter as Router } from 'react-router-dom'
import './App.css'
import SplashScreen from './pages/SplashScreen'
import './styles/tailwind.css'
import { Routes } from 'routes/Routes'
import {AppState} from "store/types";
import {useSelector} from "react-redux";
import {withLoading} from "components/withLoading";

const App = () => {
    const [isLoading, setIsLoading] = useState<boolean>(true)

    useEffect(() => {
        setTimeout(() => setIsLoading(false), 2000)
    }, [])

    const store = useSelector<AppState>(state => state )
    console.log('APP STATE = ', store)

    const debug = true
    const responsiveDebug = debug ? (
        <div className="fixed z-50 top-0 right-0 h-6 w-6 bg-black sm:bg-green-400 md:bg-blue-500 lg:bg-purple-500 xl:bg-pink-500" />
    ) : null

    const App = isLoading ? (
        <SplashScreen />
    ) : (
        <Suspense fallback={<div>Chargement...</div>}>
            <Routes />
        </Suspense>
    )

    return (
        <Router>
            {responsiveDebug}
            <div
                className="container min-h-screen h-full bg-fixed bg-gray-lighter flex flex-col items-center font-main">
                {App}
            </div>
        </Router>
    )
}

export default App
