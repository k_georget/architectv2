import axios, {
    AxiosError,
    AxiosInstance,
    AxiosRequestConfig,
    AxiosResponse,
} from 'axios'
import * as qs from 'qs'
import { PathLike } from 'fs'
import { ITokenBag } from 'services/types'

export const maderaApiConfig = {
    returnRejectedPromiseOnError: true,
    withCredentials: true,
    timeout: 30000,
    baseURL: process.env.REACT_APP_API_URL,
    version: 'v1',
    headers: {
        common: {
            /*"Cache-Control": "no-cache, no-store, must-revalidate",
            Pragma: "no-cache",*/
            'Content-Type': 'application/json',
            Accept: 'application/json',
        },
    },
    paramsSerializer: (params: PathLike) => {
        console.log(params)
        return qs.stringify(params, { indices: false })
    },
}

export class MaderaApi {
    private api: AxiosInstance

    public constructor(tokenBag?: ITokenBag | null) {
        this.api = axios.create(maderaApiConfig)

        this.api.interceptors.request.use((param: AxiosRequestConfig) => {
            if (tokenBag && !tokenBag.hasAnExpiredAccessToken()) {
                param.headers.Authorization = `Bearer ${tokenBag.accessToken}`
            }

            console.log('PARAM INTERCEPTED  ', param)
            return param
        })

        this.api.interceptors.response.use(
            MaderaApi.onRequestSuccess,
            MaderaApi.onRequestError
        )
    }

    private static onRequestSuccess(response: AxiosResponse): Array<any> | any {
        return response.data.data || response.data
    }

    private static onRequestError(error: AxiosError): void {
        // TODO SWITCH CASE ERROR HANDLER
        throw error
    }

    public get<T, R = AxiosResponse<T>>(
        url: string,
        config?: AxiosRequestConfig
    ): Promise<R> {
        return this.api.get(url, config)
    }

    public async post<P, T, R = AxiosResponse<T>>(
        url: string,
        data?: P,
        config?: AxiosRequestConfig
    ): Promise<R> {
        return await this.api.post(url, data, config)
    }
}
