import {ITokenBag, IUser, Resource} from 'services/types'
import {Car} from "services/user/Car";

export class TokenBag implements Resource {
    constructor(
        public tokenType: string,
        private _expiresIn: number,
        public accessToken: string,
        public refreshToken: string
    ) {}

    get expiresIn(): Date {
        return new Date(this._expiresIn)
    }

    public hasAnExpiredAccessToken(): boolean {
        console.log('TOKEN IS EXPIRED ? ', this._expiresIn < Date.now())
        return false;
        //return this._expiresIn < Date.now()
    }

    public static hydrate(data: any): ITokenBag {
        return Object.assign(Object.create(TokenBag.prototype), data)
    }
}
