import {ITokenBag, ResourceApi, UserCredentials} from 'services/types'
import { TokenBag } from 'services/auth/TokenBag'
import {MaderaApi} from "services/api/MaderaApi";

export class AuthApi extends MaderaApi implements ResourceApi {
    // TODO EMERGENCY 2000 => IMPLEMENT A NODEJS PROXY TO MANAGE API CREDENTIALS / CALLS

    readonly url: string = 'oauth/token'
    private readonly clientId = process.env.REACT_APP_API_CLIENT_ID
    private readonly clientSecret = process.env.REACT_APP_API_CLIENT_SECRET
    private readonly grantType = 'password'


    async signIn(userCredentials: UserCredentials): Promise<ITokenBag> {
        const data = {
            username: userCredentials.email,
            password: userCredentials.password,
            client_id: this.clientId,
            client_secret: this.clientSecret,
            grant_type: this.grantType,
        }

        return this.post(this.url, data)
            .then(this.mapper)
    }

    async signOut(): Promise<void> {
        console.log('LOGGING OUT FROM API ...')
        return await new Promise((resolve) => {
            setTimeout(() => resolve(), 500)
        })
    }

    mapper(apiRes: any): ITokenBag {
        return new TokenBag(
            apiRes.token_type,
            apiRes.expires_in,
            apiRes.access_token,
            apiRes.refresh_token
        )
    }
}
