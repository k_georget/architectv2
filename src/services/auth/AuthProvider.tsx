import React, {useContext, useEffect, useReducer} from "react";
import {AuthApi} from "services/auth/AuthApi";
import {AuthRequestParams, IUser} from "services/types";

interface AuthState {
    loading: boolean,
    error: any,
    user: IUser,
    isAuthenticated: boolean
}

interface AuthDispatch {
    login(credentials: AuthRequestParams): void
    logout(): void
}

interface AuthContext {
    state: AuthState,
    dispatch: AuthDispatch
}

const AuthStateContext = React.createContext<AuthState>({} as AuthState)
const AuthDispatchContext = React.createContext<AuthDispatch>({} as AuthDispatch)

const INITIAL_STATE = {
    isLoading: false,
    error: null,
    user: null
}

const getInitialState = () => {
    const persistedSession = localStorage.getItem('session')

    if (persistedSession !== null) {
        return JSON.parse(persistedSession)
    }

    return INITIAL_STATE
}

const authReducer = (state: any, action: any) => {
    switch (action.type) {
        case 'LOG_IN_REQUEST':
        case 'LOG_OUT_REQUEST':
            return {
                ...state,
                isLoading: true
            }
        case 'LOG_IN_SUCCESS':
            console.log("ACTION LOG IN")
            return {
                ...state,
                user: action.user,
                isLoading: false,
                error: null
            }
        case 'LOG_OUT_SUCCESS':
            console.log("ACTION LOG OUT")
            return {
                ...state,
                user: null,
                isLoading: false,
                error: null
            }
        default:
            return state
    }
}

export const AuthProvider: React.FC = ({children}) => {
    const initialState = getInitialState()
    console.log('INITIAL AUTH STATE', initialState)
    const [session, dispatch] = useReducer(authReducer, initialState)


    useEffect(() => {
        console.log("SESSION HAS CHANGED BUUUUUUDDY =>")
        console.log(session)
        localStorage.setItem('session', JSON.stringify(session))
    }, [session])

    const authenticate = async (credentials: AuthRequestParams): Promise<void> => {
        console.log("AUTHENTICATE DIY THUNK STARTING...")

        dispatch({
            type: 'LOG_IN_REQUEST'
        })

        const user = await new AuthApi().signIn(credentials)
        console.log('USER == ', user)
        dispatch({
            type: 'LOG_IN_SUCCESS',
            user
        })
    }

    const currentUser = (): IUser => {
        return { ...session.user }
    }

    const signOut = async () => {
        await new AuthApi().signOut()
        dispatch({
            type: 'LOG_OUT_SUCCESS'
        })
    }

    const isAuthenticated = () => {
        console.log('IS AUTH --- SESSION IS ---->')
        console.log(session)
        return !!session.user;
    }

    const stateToProps: AuthState = {
        loading: session.isLoading,
        error: session.error,
        user: currentUser(),
        isAuthenticated: isAuthenticated()
    }

    const dispatchToProps: AuthDispatch = {
        login: authenticate,
        logout: signOut,
    }

    return (
        <AuthStateContext.Provider value={stateToProps}>
            <AuthDispatchContext.Provider value={dispatchToProps}>
                {children}
            </AuthDispatchContext.Provider>
        </AuthStateContext.Provider>

    )
}



export const useAuthContext = (): AuthContext => {
    return {
        state: useContext(AuthStateContext),
        dispatch: useContext(AuthDispatchContext)
    }
}

