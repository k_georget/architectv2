import {Estimate} from "services/estimates/Estimate";
import {Resource, ResourceApi} from "services/types";
import {MaderaApi} from "services/api/MaderaApi";

export class EstimatesApi extends MaderaApi implements ResourceApi {
    public url = 'v1/estimates'

    mapper(apiEstimate: any): Resource {
        return new Estimate(
            apiEstimate.id,
            apiEstimate.comment,
            apiEstimate.total_price_with_VAT,
            apiEstimate.total_price_without_VAT,
            apiEstimate.customer_signature_url,
            apiEstimate.project,
            apiEstimate.customer,
            apiEstimate.house_base_id,
            apiEstimate.created_at,
            apiEstimate.updated_at,
            apiEstimate.deleted_at,
            apiEstimate.structure,
            apiEstimate.customer_validation_status,
            apiEstimate.marketing_validation_status,
            apiEstimate.design_office_validation_status,
            apiEstimate.line
        )
    }

    async getAll(): Promise<Array<Estimate>> {
        return await this.get(this.url)
            .then((estimates: any) => estimates.map(this.mapper))
            .catch(e => {
                console.log('ERROR = ', e)
                return e
            })
    }
}
