import {EstimateStatus, Line, Resource} from "services/types";


export class Estimate implements Resource {
    constructor(
        public id: number,
        public comment: string,
        public totalPriceWithVAT: number,
        public totalPriceWithoutVAT: number,
        public customerSignatureUrl: string,
        public project: any,
        public customer: any,
        public houseBaseId: number,
        public createdAt: Date,
        public updatedAt: Date,
        public deletedAt: Date | null,
        public structure: any,
        public customerValidationStatus: EstimateStatus,
        public marketingValidationStatus: EstimateStatus,
        public designOfficeValidationStatus: EstimateStatus,
        public line: Line
    ) {

    }
}

