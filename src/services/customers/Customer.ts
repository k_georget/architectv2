import {Resource} from "services/types";

export class Customer implements Resource {
    constructor(
        public id: number,
        public email: string,
        public firstName: string,
        public lastName: string,
        public birthDate: string,
        public phone: string,
        public address: string,
        public zip: string,
        public city: string,
        public createdAt: Date,
        public updatedAt: Date,
        public deletedAt: Date | null
    ) {

    }
}

