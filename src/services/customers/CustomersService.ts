import {Resource, ResourceApi} from "services/types";
import {MaderaApi} from "services/api/MaderaApi";
import {Customer} from "services/customers/Customer";

export class CustomersService extends MaderaApi implements ResourceApi {
    public url = 'v1/customers'

    mapper(apiCustomer: any): Resource {
        return new Customer(
            apiCustomer.id,
            apiCustomer.email,
            apiCustomer.first_name,
            apiCustomer.last_name,
            apiCustomer.birth_date,
            apiCustomer.phone,
            apiCustomer.address,
            apiCustomer.zip,
            apiCustomer.city ,
            apiCustomer.created_at,
            apiCustomer.updated_at,
            apiCustomer.deleted_at,
        )
    }

    async getAll(): Promise<Array<Customer>> {
        return await this.get(this.url)
            .then((customers: any) => {
                return customers.map((customer: any) => this.mapper(customer))
            })
            .catch(e => {
                console.error('ERROR = ', e)
                return e
            })
    }
}
