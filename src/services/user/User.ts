import { IUser } from 'services/types'
import {Car, ICar} from "services/user/Car";

export class User implements IUser {
    public car: ICar

    constructor(
        public email: string,
        public firstName: string,
        public lastName: string,
        public birthDate: Date,
        public phone: string,
        public salesCommissionRate: number,
        public hourlyRateWithoutVAT: number,
        public businessSite: string
    ) {
        this.car = new Car('FORD FIESTA ST')
    }

    suckDick() {
        console.log(
            '%cHello, my name is ' +
                this.firstName +
                ', and you found my secret method to s*** your d***',
            'padding: 30px; border:2px solid #7ECE2B;' +
                'background-color: #282A25; color: #7ECE2B;' +
                '#F8C775; font-family: Roboto; font-size: 15px'
        )
    }

    public static hydrate(data: any): IUser {
        const car = Car.hydrate(data.car)
        const user = Object.assign(Object.create(User.prototype), data)
        user.car = car

        return user
    }
}
