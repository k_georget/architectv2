import { ResourceApi, IUser } from 'services/types'
import { MaderaApi } from 'services/api/MaderaApi'
import { User } from 'services/user/User'

export class UserApi extends MaderaApi implements ResourceApi {
    readonly url: string = 'v1/users/me'

    async me(): Promise<IUser> {
        return this.get(this.url)
            .then(this.mapper)
            .catch((e) => {
                console.log('ERROR = ', e)
                return e
            })
    }

    mapper({ data }: any): IUser {
        return new User(
            data.email,
            data.first_name,
            data.last_name,
            data.birth_date,
            data.phone,
            data.sales_commission_rate,
            data.hourly_rate_without_VAT,
            data.business_site
        )
    }
}
