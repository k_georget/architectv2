export interface ICar {
    model: string
    start(): void
}

export class Car implements ICar {
    constructor(public model: string) {}

    start(): void {
        console.log(
            '%cVROOM VROOM MAKE THE ' + this.model,
            'padding: 30px; border:2px solid #F8C775; background-color: #282A25; color: ' +
            '#F8C775; font-family: Roboto; font-size: 15px'
        )
    }

    public static hydrate(data: any): ICar {
        return Object.assign(Object.create(Car.prototype), data)
    }
}
