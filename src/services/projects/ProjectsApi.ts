import {CreateProjectParams, GetProjectsParams, Resource, ResourceApi} from "services/types";
import {MaderaApi} from "services/api/MaderaApi";
import {Project} from "services/projects/Project";

export class ProjectsApi extends MaderaApi implements ResourceApi {
    public url = 'v1/projects'

    mapper(apiProject: any): Resource {
        return new Project(
            apiProject.id,
            apiProject.name,
            apiProject.reference,
            apiProject.comment,
            [],
            apiProject.customer_id,
            apiProject.address,
            apiProject.city,
            apiProject.zip,
            apiProject.created_at,
            apiProject.updated_at,
            apiProject.deleted_at,
            apiProject.sales_representative_id
        )
    }

    async getAll(customerId?: number): Promise<Array<Project>> {
        let params: Partial<GetProjectsParams> = {}

        if (customerId) {
            params.customer_id = customerId
        }

        return await this.get(this.url, { params })
            .then((projects: any) => {
                return projects.map((project: any) => this.mapper(project))
            })
            .catch(e => {
                console.error('ERROR = ', e)
                return e
            })
    }

    async create(data: CreateProjectParams): Promise<Array<Project>> {

        const body = {
            ...data,
            customer_id: data.customerId
        }

        console.log(body)

        return await this.post(this.url, body)
            .then(this.mapper)
            .catch(e => {
                console.error('ERROR = ', e)
                return e
            })
    }
}
