import {IEstimate, Resource} from "services/types";
import {Estimate} from "services/estimates/Estimate";


export class Project implements Resource {
    constructor(
        public id: number,
        public name: string,
        public reference: string,
        public comment: string,
        public estimates: IEstimate[],
        public customerId: number,
        public address: string,
        public city: string,
        public zip: string,
        public createdAt: Date,
        public updatedAt: Date,
        public deletedAt: Date | null,
        public salesRepresentativeId?: number,
    ) {

    }
}

