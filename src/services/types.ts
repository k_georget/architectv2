import {ICar} from "services/user/Car";

export interface Resource {}

export interface ResourceApi {
    //api: any,
    mapper: (apiRes: any) => Resource
    readonly url: string
}

export enum Status {
    Pending = 'pending',
    Validated = 'validated',
    Refused = 'refused',
}

//////////////////////////////
////////// ESTIMATES /////////
//////////////////////////////

export interface IEstimate {
    id: number
    comment: string
    totalPriceWithVAT: number
    totalPriceWithoutVAT: number
    customerSignatureUrl: string
    project: any
    customer: any
    houseBaseId: number
    createdAt: Date
    updatedAt: Date
    deletedAt: Date | null
    structure: any
    customerValidationStatus: EstimateStatus
    marketingValidationStatus: EstimateStatus
    designOfficeValidationStatus: EstimateStatus
    line: Line
}

export interface Line {
    id: number
    key: 'standard' | 'premium'
    label: string
    description: string
    createdAt: Date
    updatedAt: Date
    deletedAt: null
}

export interface EstimateStatus {
    reason: string
    createdAt: Date
    status: Status
}


/////////////////////////////////
////////// AUTH / USER //////////
/////////////////////////////////

export interface AuthRequestParams {
    email: string
    password: string
    clientId: string | number
    clientSecret: string,
    grantType: string
}

export interface UserCredentials {
    email: string
    password: string
}

export interface IUser {
    email: string
    firstName: string
    lastName: string
    birthDate: Date
    phone: string
    salesCommissionRate: number
    hourlyRateWithoutVAT: number
    businessSite: string
    suckDick(): void
    car: ICar
}

export interface ITokenBag {
    tokenType: string
    expiresIn: Date
    accessToken: string
    refreshToken: string
    hasAnExpiredAccessToken(): boolean
}

//////////////////////////////
////////// PROJECTS //////////
//////////////////////////////

export interface GetProjectsParams {
    customer_id: number
}

export interface CreateProjectParams {
    name: string,
    comment: string
    customerId: number | string,
    zip: string
    address: string
    city: string
}
