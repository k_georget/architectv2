import React from 'react'
import ReactDOM from 'react-dom'
import './styles/index.css'
import * as serviceWorker from './serviceWorker'
import AppContainer from 'AppContainer'

let alreadySaid = false
console.warn = () => {
    if (!alreadySaid) {
        console.log(
            '%cConsole warnings have been deactivated for mental health ' +
                'considerations, reactivate it asap in application entry file (src./index.tsx).',
            'padding: 30px; border:2px solid #F8C775; background-color: #282A25; color: ' +
            '#F8C775; font-family: Roboto; font-size: 15px'
        )
        alreadySaid = true
    }
}

ReactDOM.render(<AppContainer />, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register()
