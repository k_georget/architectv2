import React from 'react';
import bgImage from 'images/splash_bg.jpg';
import logo from 'images/main_logo_text.png';
import LoadingIndicator from "components/loadingIndicators/LoadingIndicator";


const SplashScreen: React.FC = () => {
    return (<>
            <div className="fixed bg-fixed h-screen w-screen opacity-50"
                 style={{
                     background: 'linear-gradient(to bottom, #90cdf4, transparent 40%, transparent)'
                 }}
            />
            <div className="bg-fixed bg-cover h-screen w-screen opacity-100"
                 style={{
                     backgroundImage: "url(" + bgImage + ")",
                 }}
            >
                <div className="flex-col h-full justify-around items-center flex">
                    <img className="w-1/2 z-10" src={logo} alt="Logo Madera"/>
                    <LoadingIndicator
                        position="fixed"
                        color="gray-lighter"
                        insetY="10/12"
                    />
                </div>

            </div>
    </>)
}

export default SplashScreen;
