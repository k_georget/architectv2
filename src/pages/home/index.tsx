import React from 'react'
import { useHistory, Switch } from 'react-router-dom'
import BottomNavigator from 'components/navigation/BottomNavigator/BottomNavigator'
import homeRoutes from './routes'
import PrivateRoute from 'routes/PrivateRoute'
import { AppRoute } from 'routes/Routes'

const Home: React.FC = () => {
    return (
        <>
            <div className="container relative border-2 border-blue-400">
                <Switch>
                    {homeRoutes.map((route: AppRoute) => (
                        <PrivateRoute key={route.path} path={route.path}>
                            {route.page}
                        </PrivateRoute>
                    ))}
                </Switch>
                <div className="container m-auto inset-x-0 bg-white fixed h-16 bottom-0">
                    <BottomNavigator
                        routes={homeRoutes}
                        className="w-full h-full"
                        tabLabelClassName="text-primary font-main text-sm"
                    />
                </div>
            </div>
        </>
    )
}

export default Home
