import {AppRoute} from "routes/Routes";
import React from "react";
import Search from "pages/home/search/Search";
import Estimates from "pages/home/estimates";

const routes: Array<AppRoute> = [
    {
        label: 'Devis',
        path: '/estimates',
        page: <Estimates/>,
        isPrivate: true,
        exact: true
    },
    {
        label: 'Rechercher',
        path: '/search',
        page: <Search/>,
        isPrivate: true,
        exact: true
    },
    {
        label: 'Catalogue',
        path: '/catalog',
        page: <div className="absolute w-full h-full border-2 border-black bg-blue-400">Catalogue</div>,
        isPrivate: true
    },
    {
        label: 'Profil',
        path: '/profile',
        page: <div>Profil</div>,
        isPrivate: true,
    },
];

export default routes
