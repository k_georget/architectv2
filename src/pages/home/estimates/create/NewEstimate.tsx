import AppText from 'components/common/AppText'
import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { Customer } from 'services/customers/Customer'
import { CustomersService } from 'services/customers/CustomersService'
import { ProjectsApi } from 'services/projects/ProjectsApi'
import { Project } from 'services/projects/Project'
import AddButton from 'components/buttons/AddButton'
import { useSelector } from 'react-redux'
import { tokenBagSelector } from 'store/selectors/auth'
import BaseButton from 'components/buttons/BaseButton'
import Header from 'components/headers/Header'
import BaseSelect from 'components/select/BaseSelect'
import NextButton from 'components/buttons/NextButton'
import App from "App";

const NewEstimate: React.FC = ({ children }) => {
    const [customers, setCustomers] = useState<Array<Customer>>([])
    const [selectedCustomerId, setSelectedCustomerId] = useState<
        number | 'default'
    >('default')
    const [selectedProjectId, setSelectedProjectId] = useState<
        number | 'default'
    >('default')
    const [projects, setProjects] = useState<Array<Project>>([])
    const [defaultProjectLabel, setDefaultProjectLabel] = useState<string>(
        "Choissisez d'abord un client"
    )

    const tokenBag = useSelector(tokenBagSelector)

    useEffect(() => {
        new CustomersService(tokenBag).getAll().then((customers) => {
            setCustomers(customers)
            /*if (customers.length) {
                setSelectedCustomerId(customers[0].id)
            }*/
        })
    }, [])

    useEffect(() => {
        selectedCustomerId !== 'default' &&
            new ProjectsApi(tokenBag)
                .getAll(selectedCustomerId)
                .then((projects) => {
                    setProjects(projects)
                    if (projects.length) {
                        setDefaultProjectLabel('Choisissez un projet')
                        setSelectedProjectId(projects[0].id)
                    } else {
                        setDefaultProjectLabel('Aucun projet pour ce client')
                        setSelectedProjectId('default')
                    }
                })
    }, [selectedCustomerId])

    return (
        <div>
            <Header
                height="16"
                smHeight="24"
                mdHeight="24"
                lgHeight="40"
                xlHeight="64"
                center={
                    <AppText size="xl">
                        Nouveau devis
                    </AppText>
                }
                centerClassName="overflow-hidden justify-center text-lg lg:text-xl xl:text-2xl font-title"
                left={
                    <Link to="/estimates">
                        <BaseButton label="Annuler" actionType="secondary" />
                    </Link>
                }
                leftClassName="ml-4"
            />
            <section className="w-11/12 m-auto my-6 p-4 bg-gray-lightest shadow-md rounded">
                <AppText size="lg" color="secondary">
                    <strong>Bienvenu dans l'assistant de création de devis. :)</strong>
                </AppText>
                <br/>
                <AppText>
                    Avant de lancer l'Architecte, vous devez <strong>sélectionner un client et un projet.</strong>
                </AppText>
                <br/>
                <AppText>
                    Choisissez un de vos clients dans la liste déroulante ci-dessous et sélectionnez ensuite
                    un des projets du client.
                </AppText>
                <br/>
                <AppText>
                    Si aucun projet n'existe encore pour le client sélectionné,
                    créez un nouveau projet en appuyant sur <strong>"Nouveau projet"</strong>.
                    <br/><br/>
                    Une fois le projet choisi, c'est parti ! <strong>Cliquez sur "Démarrer l'architecte"</strong>
                </AppText>
                <br/>
                <AppText>
                    Si vous ne trouvez pas votre client dans la liste, cliquez sur "Client introuvable ?" pour
                    démarrer la procédure de dépannage.
                </AppText>
            </section>
            <form
                className="w-11/12 m-auto h-8/12vh"
                onSubmit={(e) => {
                    console.log(e)
                }}>
                <div className="w-full px-3 pt-5">
                    <AppText size="lg">Client</AppText>

                    <div className="flex flex-row justify-between">
                        <div className="w-8/12">
                            <BaseSelect
                                value={selectedCustomerId}
                                onChange={(e) =>
                                    setSelectedCustomerId(
                                        parseInt(e.target.value)
                                    )
                                }>
                                {[
                                    <option disabled value="default">
                                        Choisissez votre client
                                    </option>,
                                    ...customers.map((customer) => (
                                        <option
                                            key={customer.id}
                                            value={customer.id}>
                                            {customer.firstName}{' '}
                                            {customer.lastName}
                                        </option>
                                    )),
                                ]}
                            </BaseSelect>
                        </div>
                        <div className="w-4/12 flex items-center justify-end">
                            <BaseButton
                                actionType="secondary"
                                onClick={(e) => {
                                    alert('PAS DE BOL :/')
                                }}
                                label="Client introuvable ?"
                            />
                        </div>
                    </div>
                </div>

                {/* Project select */}
                <div className="w-full px-3 pt-5">
                    <AppText size="lg">Projet</AppText>

                    <div className="flex flex-row justify-between">
                        <div className="w-8/12">
                            <BaseSelect
                                value={selectedProjectId}
                                onChange={(e) =>
                                    setSelectedProjectId(
                                        parseInt(e.target.value)
                                    )
                                }>
                                {[
                                    <option value="default" disabled>
                                        {defaultProjectLabel}
                                    </option>,
                                    ...projects.map((project) => (
                                        <option
                                            key={project.id}
                                            value={project.id}>
                                            {project.name} ({project.reference})
                                        </option>
                                    )),
                                ]}
                            </BaseSelect>
                        </div>
                        <div className="w-4/12 flex items-center justify-end">
                            <BaseButton
                                onClick={(e) => {
                                    //e.preventDefault()
                                }}
                                disabled={selectedCustomerId === 'default'}
                                label="Nouveau projet"
                            />
                        </div>
                    </div>
                </div>

                {/* Footer */}
                <div className="flex flex-row items-center justify-end h-4/12 mr-4">
                    <AppText
                        className="pr-2 font-button"
                        size="md"
                        color="primary">
                        Démarrer l'Architecte
                    </AppText>
                    <NextButton
                        disabled={
                            selectedCustomerId === 'default' ||
                            selectedProjectId === 'default'
                        }
                    />
                </div>
            </form>
        </div>
    )
}

/*new ProjectsApi(tokenBag)
    .create({
    name: 'Cabane enfants',
    comment:
        'Cabane environ 35m2 pour les enfants de Mme Gonzalez',
    customerId: 1,
    zip: '74000',
    address: '34, rue de la verrière',
    city: 'Annecy',
    })
    .then((res) => console.log(res))*/

export default NewEstimate
