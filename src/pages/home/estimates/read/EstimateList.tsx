import React, { useCallback, useEffect, useState } from 'react'
import { useRouteMatch, useHistory } from 'react-router-dom'
import { Estimate } from 'services/estimates/Estimate'
import AppText from 'components/common/AppText'
import EstimateItem from 'components/estimates/EstimateItem'
import { EstimatesApi } from 'services/estimates/EstimatesApi'
import AddButton from 'components/buttons/AddButton'
import { Status } from 'services/types'
import { useDispatch, useSelector } from 'react-redux'
import { fetchAllEstimates } from 'store/actions/estimates'
import {
    allEstimatesSelector,
    isFetchingEstimates,
} from 'store/selectors/estimates'
import { withLoading } from 'components/withLoading'
import { AppState } from 'store/types'
import Header from 'components/headers/Header'
import BaseButton from 'components/buttons/BaseButton'
import { requestSignOut } from 'store/actions/auth'

const EstimateList: React.FC = () => {
    const [buttonTransform, setButtonTransform] = useState<string>('')

    let { path, url } = useRouteMatch()
    let history = useHistory()

    const dispatch = useDispatch()

    const signOut = useCallback(() => dispatch(requestSignOut()), [dispatch])

    const estimates = useSelector(allEstimatesSelector)
    const isLoading = useSelector(isFetchingEstimates)

    const fetchEstimates = useCallback(() => dispatch(fetchAllEstimates()), [
        dispatch,
    ])

    useEffect(() => {
        fetchEstimates()
    }, [])

    const onNewEstimateClick = () => {
        history.push('/estimates/new')
    }

    const estimatesFilterForm = (
        <div className="w-1/3 px-3">
            <AppText size="md">Statut du devis</AppText>

            <div className="relative">
                <select
                    onChange={(e) => console.log(e.target.value)}
                    className="w-full bg-white border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none border-gray-500"
                    id="grid-state">
                    <option>Tous</option>
                    <option>En commande</option>
                    <option>En cours de validation</option>
                    <option>Refusé</option>
                </select>
                <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                    <svg
                        className="fill-current h-4 w-4"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 20 20">
                        <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" />
                    </svg>
                </div>
            </div>
        </div>
    )

    /* const separator = (
        <div className="h-px my-3 bg-gray-400 w-full"/>
    )
*/

    const estimateList = () => {
        if (!estimates.length) {
            return 'Aucun devis réalisé'
        }

        return estimates.map((estimate: Estimate) => {
            const {
                id,
                customer,
                project,
                line,
                totalPriceWithVAT,
                customerValidationStatus,
                marketingValidationStatus,
                designOfficeValidationStatus,
            } = estimate

            const getStatusSummary = (): Status => {
                const statuses = [
                    customerValidationStatus.status,
                    marketingValidationStatus.status,
                    designOfficeValidationStatus.status,
                ]

                if (statuses.includes(Status.Refused)) {
                    return Status.Refused
                }

                if (
                    statuses.filter((status) => status === Status.Validated)
                        .length === statuses.length
                ) {
                    return Status.Validated
                }

                return Status.Pending
            }

            return (
                <EstimateItem
                    key={id}
                    className="my-2"
                    customerFullName={
                        customer.first_name + ' ' + customer.last_name
                    }
                    projectName={project.name}
                    line={line.label}
                    totalPriceWithVAT={totalPriceWithVAT}
                    statusSummary={getStatusSummary()}
                    customerValidationStatus={customerValidationStatus.status}
                    marketingValidationStatus={marketingValidationStatus.status}
                    designOfficeValidationStatus={
                        designOfficeValidationStatus.status
                    }
                />
            )
        })
    }

    return (
        <>
            <div className="">
                <Header
                    height="16"
                    smHeight="24"
                    mdHeight="24"
                    lgHeight="40"
                    xlHeight="64"
                    center={<AppText size="xl">Mes devis</AppText>}
                    centerClassName="justify-center text-lg lg:text-xl xl:text-2xl font-title"
                    right={
                        <BaseButton
                            label="Déconnexion"
                            actionType="secondary"
                            onClick={signOut}
                        />
                    }
                />
                <div className="flex flex-col justify-around p-4">
                    <div className="flex flex-row justify-between">
                        {estimatesFilterForm}
                        <AddButton onClick={onNewEstimateClick} />
                    </div>
                    <div className="border-t-2 border-grey-500 overflow-y-scroll h-screen mt-2 px-2 pt-2 pb-64">
                        {estimateList()}
                    </div>
                </div>
            </div>
        </>
    )
}

export default withLoading({
    loadingWhat: 'estimates',
    insetY: '1/2',
    size: '1.5vh',
    color: 'primary',
})(EstimateList)
