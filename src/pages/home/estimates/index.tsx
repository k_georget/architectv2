import React from 'react';
import {Switch} from "react-router-dom";
import estimatesRoutes from "pages/home/estimates/routes";
import {AppRoute} from "routes/Routes";
import PrivateRoute from "routes/PrivateRoute";

const Estimates: React.FC = () => {
    return (
        <div className="relative">
            <Switch>
                {
                    estimatesRoutes.map((route: AppRoute) => (
                        <PrivateRoute
                            key={route.path}
                            path={route.path}
                        >
                            {route.page}
                        </PrivateRoute>
                    ))
                }
            </Switch>
        </div>
    )
}

export default Estimates;
