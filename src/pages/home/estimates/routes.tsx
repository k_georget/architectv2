import React from "react";
import {AppRoute} from "routes/Routes";
import EstimateList from "pages/home/estimates/read/EstimateList";
import NewEstimate from "pages/home/estimates/create/NewEstimate";

const routes: Array<AppRoute> = [
    {
        label: 'Nouveau Devis',
        path: '/estimates/new',
        page: <NewEstimate/>,
        isPrivate: true
    },
    {
        label: 'Mes Devis',
        path: '/estimates',
        page: <EstimateList/>,
        exact: true,
        isPrivate: true
    }
];

export default routes
