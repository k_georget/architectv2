import React, { useCallback } from 'react'
import { TextInputField } from 'components/forms/TextInputField'
import {
    Formik,
    Form,
    FormikErrors,
    FormikProps,
    FormikValues,
    FormikHelpers,
} from 'formik'
import { useDispatch } from 'react-redux'
import { WithLoading, withLoading } from 'components/withLoading'
import { requestSignIn } from 'store/actions/auth'
import { UserCredentials } from 'services/types'
import AppText from 'components/common/AppText'
import NextButton from 'components/buttons/NextButton'
import bgImage from 'images/splash_bg.jpg'
import logo from 'images/main_logo_text.png'
import { string, object, ValidationError } from 'yup'

const loginSchema = object({
    email: string()
        .email('Email invalide.')
        .required('Veuillez saisir votre email.'),
    password: string()
        .required('Saissez votre mot de passe.')
        .max(15, 'Mot de passe trop long'),
})

const Login = () => {
    const dispatch = useDispatch()
    const signIn = useCallback(
        (credentials: UserCredentials) => dispatch(requestSignIn(credentials)),
        [dispatch]
    )

    const submitForm = (
        values: UserCredentials,
        actions: FormikHelpers<any>
    ) => {
        signIn(values)
    }

    const initialValues: UserCredentials = {
        email: '',
        password: '',
    }

    const emailErrors = (email: string): string | string[] => {
        if (!email) {
            return 'Veuillez saisir votre email.'
        }

        if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(email)) {
            return 'Email invalide.'
        }

        return ''
    }

    const passwordErrors = (password: string): string | string[] => {
        if (!password) {
            return 'Saissez votre mot de passe.'
        }

        return ''
    }

    return (
        <>
            <div className="login-bg fixed bg-fixed h-screen w-screen">
                <div className="fixed bg-secondary opacity-25 z-10 h-screen w-screen" />
                <div
                    className="fixed bg-fixed bg-cover h-screen w-screen opacity-10"
                    style={{
                        backgroundImage: 'url(' + bgImage + ')',
                    }}
                />
            </div>

            <Formik
                initialValues={initialValues}
                validate={async (values) => {
                    const errors: FormikErrors<any> = {}
                    await loginSchema.validate(values, { abortEarly: false })
                        .catch(e => e.inner.forEach((error: ValidationError) => errors[error.path] = error.message))

                    return errors
                }}
                onSubmit={(values, actions) => submitForm(values, actions)}>
                {(props: FormikProps<FormikValues>) => (
                    <Form
                        style={{ backgroundColor: 'rgba(255,255,255,0.5)' }}
                        className="
                        flex
                        z-20
                        flex-col
                        items-center
                        shadow-2xl
                        rounded-md

                        h-9/12vh sm:h-9/12vh md:h-7/12vh lg:h-7/12vh xl:h-7/12vh 2xl:h-7/12vh
                        w-11/12 sm:w-3/4 md:w-1/2 lg:w-1/2 xl:w-1/2 2xl:w-1/2
                        m-auto
                        ">
                        <div className="my-4 w-full h-5/12 flex flex-col bg-transparent justify-around items-center">
                            <div className="w-56 h-56 p-12 z-10 rounded-full bg-white">
                                <img
                                    className="w-full z-10"
                                    src={logo}
                                    alt="Logo Madera"
                                />
                            </div>
                            <AppText
                                className="font-title"
                                size="xl"
                                color="gray-darkest">
                                Bienvenue
                            </AppText>
                        </div>
                        <div className="flex flex-col w-8/12 h-7/12 justify-between">
                            <div className="flex flex-col h-7/12 justify-between">
                                <TextInputField
                                    label="Email"
                                    type="email"
                                    name="email"
                                />
                                <div>
                                    <TextInputField
                                        label="Mot de passe"
                                        type="password"
                                        name="password"
                                    />
                                    <AppText
                                        className="pt-3"
                                        size="sm"
                                        color="secondary">
                                        Mot de passe oublié ?
                                    </AppText>
                                </div>
                            </div>
                            <div className="flex self-end pb-6 w-8/12 flex-row items-center justify-end">
                                <AppText
                                    className="pr-2 font-button"
                                    size="md"
                                    color="primary">
                                    Connexion
                                </AppText>
                                <NextButton
                                    disabled={props.isSubmitting}
                                />
                            </div>
                        </div>
                    </Form>
                )}
            </Formik>
        </>
    )
}

export default withLoading({
    loadingWhat: 'auth',
    color: 'primary',
    position: 'absolute',
    insetY: '10/12',
})(Login)
