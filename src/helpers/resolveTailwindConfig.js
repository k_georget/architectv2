import resolveConfig from 'tailwindcss/resolveConfig'
import * as tailwindConfig from '../tailwindConfigSync'

export const resolveTailwindConfig = () => {
     return resolveConfig(tailwindConfig)
}

export const themeColorToHexColor = (themeColor) => {
    const appColors = resolveTailwindConfig().theme.colors

    if (appColors.hasOwnProperty(themeColor)) {
        return appColors[themeColor]
    }

    console.warn(
        themeColor +
            ' is not a valid app theme color. Valid theme colors: ' +
            Object.keys(appColors)
    )
}
