module.exports = {
    root: true,
    extends: "@react-native-community",
    parser: "@typescript-eslint/parser",
    plugins: ["@typescript-eslint"],
    parserOptions: {
        project: "./tsconfig.json",
    },
    rules: {
        indent: ["error", 4, { "SwitchCase": 1 }],
        semi: "off",
        "no-unused-vars": "off"
    },
}
